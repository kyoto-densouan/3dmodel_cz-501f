# README #

1/3スケールのSHARP X-1用FDDユニット風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。 

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。 

***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1985年

## 参考資料

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-501f/raw/ea22dd5af93df40d108d072b7f94edf2f26b18e6/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-501f/raw/ea22dd5af93df40d108d072b7f94edf2f26b18e6/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-501f/raw/ea22dd5af93df40d108d072b7f94edf2f26b18e6/ExampleImage.jpg)
